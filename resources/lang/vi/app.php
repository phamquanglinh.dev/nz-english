<?php

return [
    'module' => [
        'user' => 'Nhân sự',
        'branch' => 'Chi nhánh',
        'student' => 'Học sinh',
        'classroom' => 'Lớp học',
        'handle-day' => 'Bù ngày',
        'package' => 'Gói',
        'invoice' => 'Hóa đơn',
        'renew' => 'Gia hạn',
    ],
    'label' => [
        'user' => [
            'name' => 'Tên',
            'email' => 'Email',
            'password' => 'Mật khẩu',
            'branch' => 'Chi nhánh',
            'role' => 'Quyền',
        ],
        'branch' => [
            'name' => 'Tên chi nhánh',
            'description' => 'Mô tả',
            'created_at' => 'Thời gian tạo'
        ],
        'student' => [
            'name' => 'Tên',
            'avatar' => 'Ảnh đại diện',
            'birthday' => 'Sinh nhật',
            'phone' => 'Số điện thoại',
            'classroom_id' => 'Lớp đang học',
            'package_id' => 'Gói đã đăng ký',
            'register_at' => 'Ngày bắt đầu đăng ký',
            'started_at' => 'Ngày bắt đầu gói',
            'ended_at' => 'Ngày kết thúc gói',
            'archived_at' => 'Ngày bảo lưu',
            'note' => 'Ghi chú',
            'status' => 'Trạng thái',
            'invoice_status' => 'Thanh toán',
            'payment_date' => 'Ngày hẹn đóng',
            'status_options' => [
                'customer' => 'Đang tư vấn',
                'learning' => 'Đang học',
                'stop' => 'Đã ngừng học',
                'archived' => 'Đang bảo lưu',
            ],
            'invoice_status_options' => [
                'invoice_sent' => 'Đã lựa chọn gói, đóng tiền',
                'invoice_renew' => 'Đã đóng tiền gia hạn'
            ],
            'extra_information' => 'Thông tin thêm',
            'update_ended_at' => 'Cập nhật ngày kết thúc dựa theo ngày bắt đầu (áp dụng khi chỉnh sửa học sinh)',
            'remaining_day' => 'Số ngày còn lại',
            'package' => [
                'original_weeks' => 'Số tuần',
                'original_fee' => 'Học phí',
                'paid' => 'Đã đóng',
                'unpaid' => 'Còn lại',
            ],
            'invoices' => 'Hóa đơn',
            'send' => 'Gửi',
            'sale_day' =>  'Ngày tư vấn'
        ],

        'classroom' => [
            'name' => 'Tên lớp',
            'note' => 'Ghi chú',
            'schedules' => 'Lịch học',
        ],
        'handle_day' => [
            'classroom_ids' => 'Lớp học',
            'student_ids' => 'Học sinh',
            'bonus_days' => 'Số buổi tặng'
        ],
        'pack' => [
            'name' => 'Tên gói',
            'original_fee' => 'Học phí',
            'original_weeks' => 'Số tuần',
        ],
        'invoice' => [
            'amount' => 'Số tiền',
            'created_by' => 'Người tạo',
            'created_time' => 'Thời gian tạo',
            'student_id' => 'Học sinh',
            'method' => 'Phương thức',
            'method_options' => [
                'cash' => 'Tiền mặt',
                'bank' => 'Chuyển khoản'
            ],
            'note' => 'Ghi chú',
            'out_of_value' => 'Vượt quá số tiền còn lại cần đóng'
        ],
        'renew' => [
            'student_id' => 'Học sinh cần gia hạn ',
            'package_id' => 'Gói (áp dụng cho thao tác THAY ĐỔI GÓI MỚI)',
            'method' => 'Phương thức gia hạn',
            'method_options' => [
                'renew' => 'Gia hạn gói trước đó',
                'replace' => 'Thay đổi gói mới'
            ]
        ]
    ],
    'extra_information' => [
        'label' => 'Tên thông tin',
        'value' => 'Giá trị',
        'new_item_label' => 'Thêm thông tin mới'
    ],
    'schedule' => [
        'week_day' => 'Ngày (Thứ)',
        'start_time' => 'Thời gian bắt đầu',
        'end_time' => 'Thời gian kết thúc',
        'new_item_label' => 'Thêm lịch học mới'
    ],

    'invoice' => [
        'label' => 'Hóa đơn',
        'new_item_label' => 'Thêm hóa đơn mới'
    ],

    'week_day' => [
        'monday' => 'Thứ hai',
        'tuesday' => 'Thứ ba',
        'wednesday' => 'Thứ tư',
        'thursday' => 'Thứ năm',
        'friday' => 'Thứ sáu',
        'saturday' => 'Thứ bảy',
        'sunday' => 'Chủ nhật',
    ],
    'action' => [
        'student' => [
            'payment' => 'Thêm hoá đơn',
            'switch_to_old_student' => 'Chuyển thành học sinh cũ',
            'renew' => 'Gia hạn',
            'mark_customer_payment' => 'Đánh dấu đã tư vấn và đóng tiền',
            'mark_student_payment' => 'Đánh dấu có cập nhật thanh toán',
            'mark_student_renew' => 'Đánh dấu có cập nhật renew',
        ]
    ],
    'admin' => 'Quản trị viên',
    'staff' => 'Nhân viên',
    'manager' => 'Quản lý',
];
