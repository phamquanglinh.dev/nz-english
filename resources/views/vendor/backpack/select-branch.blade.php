@php
    use App\Models\Branch;
    use App\Models\User;
@endphp
@php
    /**
* @var Branch[] $branches
 * @var User $user
 */

     $layoutName = $user['branch'] == null ? 'layouts.plain' :'blank';

     $branch = Branch::query()->where('id',  $user['branch'])->first();


     if (! $branch) {
        $layoutName = 'layouts.plain';
     }
@endphp

@extends(backpack_view($layoutName))


@section('content')
    <div class="container">
        <div class="mb-3 text-center">Lựa chọn chi nhánh</div>
        <div class="row">
            @foreach($branches as $branch)
                <div class="col-md-4 col-sm-6 col-12">
                    <div class="p-3 text-center border rounded mb-2">
                        <a class="nav-link m-0" href="{{url('admin/branch/choose/'.$branch->id)}}">
                            <div>{{$branch->name}}</div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
