@php use App\Models\Branch; @endphp
{{-- This file is used to store topbar (left) items --}}

{{-- <li class="nav-item px-3"><a class="nav-link" href="#">Dashboard</a></li>
<li class="nav-item px-3"><a class="nav-link" href="#">Users</a></li>
 --}}
@if(isAdmin())
    <li class="nav-item px-3"><a class="nav-link" href="{{ backpack_url('branch') }}">{{__('app.module.branch')}}</a></li>
    <li class="nav-item px-3"><a class="nav-link" href="{{url('admin/branch/select')}}">
            {{Branch::getCurrentLabel()}} <i class="las la-exchange-alt"></i>
        </a></li>

@endif
