{{-- This file is used to store sidebar items, inside the Backpack admin panel --}}
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i
                class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>


@if(isAdmin())
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i
                    class="nav-icon la la-user"></i> {{ trans('app.module.user') }}</a></li>
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('branch') }}"><i
                class="nav-icon la la-building"></i> {{ trans('app.module.branch') }}</a></li>
@endif



<li class="nav-item"><a class="nav-link" href="{{ backpack_url('student') }}"><i class="nav-icon la la-user-graduate"></i> {{__('app.module.student')}}</a></li>

<li class="nav-item"><a class="nav-link" href="{{ backpack_url('classroom') }}"><i class="nav-icon la la-chalkboard-teacher"></i> {{__('app.module.classroom')}}</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('package') }}"><i class="nav-icon la la-cubes"></i> {{__('app.module.package')}}</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('handle-day/create') }}"><i class="nav-icon la la-plus-circle"></i> {{__('app.module.handle-day')}}</a></li>

<li class="nav-item"><a class="nav-link" href="{{ backpack_url('invoice') }}"><i class="nav-icon la la-money-bill"></i> {{__('app.module.invoice')}}</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('renew/create') }}"><i class="nav-icon la la-radiation"></i> {{__('app.module.renew')}}</a></li>

{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('student-log') }}"><i class="nav-icon la la-question"></i> Student logs</a></li>--}}
