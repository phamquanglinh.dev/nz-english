@php use App\Models\Package;use App\Models\Student; @endphp
@extends(backpack_view('blank'))
@section('content')
    <div class="row">
        <div class="col-md-3">
            <a href="{{url('/admin/student')}}" class="nav-link m-0 text-dark">
                <div class="card mb-3  border-start-0 ">
                    <div class="ribbon ribbon-top bg-danger p-1">
                        <i class="la la-user fs-3"></i>
                    </div>

                    <div class="card-status-start bg-danger"></div>
                    <div class="card-body">
                        <div class="subheader ">Học sinh.</div>

                        <div class="h1 mb-3 ">{{Student::query()->count()}}</div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="{{url('/admin/classroom')}}" class="nav-link m-0 text-dark">
                <div class="card mb-3  border-start-0 ">
                    <div class="ribbon ribbon-top bg-primary p-1">
                        <i class="la la-chalkboard-teacher fs-3"></i>
                    </div>

                    <div class="card-status-start bg-primary"></div>
                    <div class="card-body">
                        <div class="subheader ">Lớp học.</div>

                        <div class="h1 mb-3 ">{{\App\Models\Classroom::query()->count()}}</div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="{{url('/admin/package')}}" class="nav-link m-0 text-dark">
                <div class="card mb-3  border-start-0 ">
                    <div class="ribbon ribbon-top bg-warning p-1">
                        <i class="la la-cubes fs-3"></i>
                    </div>

                    <div class="card-status-start bg-success"></div>
                    <div class="card-body">
                        <div class="subheader ">Gói học.</div>

                        <div class="h1 mb-3 ">{{Package::query()->count()}}</div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="{{url('/admin/package')}}" class="nav-link m-0 text-dark">
                <div class="card mb-3 border-start-0 ">
                    <div class="ribbon ribbon-top bg-success p-1">
                        <i class="la la-file-invoice-dollar fs-3"></i>
                    </div>

                    <div class="card-status-start bg-success"></div>
                    <div class="card-body">
                        <div class="subheader ">Hoá đơn</div>

                        <div class="h1 mb-3 ">{{\App\Models\Invoice::query()->count()}}</div>
                    </div>
                </div>
            </a>
        </div>
    </div>
@endsection
