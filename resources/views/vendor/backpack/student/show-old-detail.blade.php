@php use App\Models\Student;use App\Models\StudentLog; @endphp
@php
    /**
* @var Student $student
 * @var StudentLog[] $studentLogs
 */
@endphp
<div class="container-fluid py-2">
    <div class="row">
        <div class="col-md-7">
            <div class="row">
                <div class="mb-2">
                    <img alt="" src="{{$student->avatar}}" style="max-width: 8rem" class="rounded">
                </div>
                <div class="ml-3">
                    <div class="nav-link font-weight-bold h5">{{$student->name}}</div>
                    <div class="nav-link">{{__('app.label.student.birthday')}}
                        : {{$student->getDetailBirthdayAttribute()}}</div>
                    <div class="nav-link">{{__('app.label.student.phone')}}: {{$student->phone}}</div>
                    <div class="nav-link">{{__('app.label.student.classroom_id')}}: {{$student->classroom?->name}}</div>
                    <div class="nav-link">{{__('app.label.student.package_id')}}: {{$student->package?->name}}</div>
                    <div class="nav-link">{{__('app.label.student.register_at')}}
                        : {{$student->getDetailRegisterAtAttribute()}}</div>
                    <div class="nav-link">{{__('app.label.student.started_at')}}
                        : {{$student->getDetailStartedAtAttribute()}}</div>
                    <div class="nav-link">{{__('app.label.student.ended_at')}}: <span
                            class="font-weight-bold {{$student->getRemainingTextColor()}}">{{$student->getDetailEndedAtAttribute()}}</span>
                    </div>
                    <div class="nav-link">{{__('app.label.student.remaining_day')}}: <span
                            class="font-weight-bold {{$student->getRemainingTextColor()}}">{{$student->getDetailRemainingDay()}}</span>
                    </div>
                </div>
                <div class="ml-3 mt-4">
                    <div class="nav-link">
                        {{__('app.label.student.package.original_weeks')}}
                        : {{$student->package?->original_weeks*($student->Renews()->count()+1)}}
                        <div class="small text-success">({{$student->package?->original_weeks}} + {{$student->Renews()->count()}} lần renew)</div>
                    </div>
                    <div class="nav-link">
                        {{__('app.label.student.package.original_fee')}}
                        : {{number_format($student->package?->original_fee*($student->Renews()->count()+1))}} đ
                        <div class="small text-success">({{number_format($student->package?->original_fee)}} đ + {{$student->Renews()->count()}} lần renew)</div>
                    </div>
                    <div class="nav-link">
                        {{__('app.label.student.package.paid')}}
                        : {{number_format($student->Invoices()->where('package_id',$student->package_id)->sum('amount'))}}
                        đ
                    </div>
                    <div class="nav-link">
                        {{__('app.label.student.package.unpaid')}} : {{number_format($student->unPaidAmount())}} đ
                    </div>
                    <div class="nav-link">
                        {{__('app.label.student.payment_date')}} : <span
                            class="font-weight-bold {{$student->getPaymentDateTextColor()}}">{{\Carbon\Carbon::parse($student->payment_date)->isoFormat('DD-MM-YYYY')}}</span>
                    </div>
                    <div>
                        <a href="{{url("admin/student/restore/{$student->id}")}}" class="nav-link">
                            <span>Khôi phục học sinh này</span>
                        </a>
                    </div>
                    <div class="d-flex">

                    </div>

                </div>
            </div>
            <div class="font-weight-bold mb-3">Ghi chú:</div>
            <div>{{$student->note}}</div>
        </div>
        <div class="col-md-5">
            <div class="font-weight-bold mb-3">Nhật ký hoạt động:</div>
            <div class="my-2" style="overflow-y:scroll;max-height: 250px">
                @foreach($studentLogs as $studentLog)
                    <div class="border rounded p-2 mb-3 bg-white">
                        <div class="mb-1">
                            <a href="{{url('admin/user/'.$studentLog->User?->id."/show")}}"
                               class="bold">{{$studentLog->User?->name}}</a>
                            đã
                            <b class="text-primary">{{$studentLog->type}}</b>
                            @if($studentLog->confirmed == 0)
                                <a href="{{isManagerOfHigher() ? url('admin/student-log/confirmed/'.$studentLog->id) : "#"}}">
                                    <i class="las la-exclamation-circle text-warning"></i>
                                </a>
                            @else
                                <i class="las la-check-circle text-success"></i>
                            @endif
                        </div>
                        <div class="mb-1">@if($studentLog->note!='')
                                "{{$studentLog->note}}"
                            @endif</div>
                        <div>Thời gian: {{$studentLog->created_at}}</div>
                    </div>
                @endforeach
            </div>
            <hr>
            <form action="{{url('admin/student/mark/'.$student->id)}}" method="POST">
                @csrf
                <select class="form-control" name="type">
                    @if($student->status == Student::STATUS_CUSTOMER)
                        <option
                            value="mark_customer_payment">{{__('app.action.student.mark_customer_payment')}}</option>
                    @else
                        <option value="mark_student_payment">{{__('app.action.student.mark_student_payment')}}</option>
                        <option value="mark_student_renew">{{__('app.action.student.mark_student_renew')}}</option>
                    @endif
                </select>
                <label class="mt-2">{{__('app.label.student.note')}}</label>
                <textarea name="note" cols="3" class="form-control "
                          placeholder="{{__('app.placeholder.note')}}"></textarea>
                <button class="btn btn-success mt-2">{{__('app.label.student.send')}}</button>
            </form>
        </div>
    </div>
</div>
