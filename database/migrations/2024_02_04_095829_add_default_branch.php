<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddDefaultBranch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('branches')->whereIn('id', [1, 2, 3])->delete();
        DB::table('branches')->insert([
            'id' => 1,
            'name' => 'Chi nhánh Bình Dương',
            'description' => 'Chi nhánh Bình Dương',
        ]);
        DB::table('branches')->insert([
            'id' => 2,
            'name' => 'Chi nhánh Hà Nội',
            'description' => 'Chi nhánh Hà Nội',
        ]);
        DB::table('branches')->insert([
            'id' => 3,
            'name' => 'Chi nhánh TP.HCM',
            'description' => 'Chi nhánh TP.HCM',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
