<?php

use App\Models\Student;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('avatar')->default('https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_640.png');
            $table->date('birthday')->nullable();
            $table->string('phone')->nullable();
            $table->integer('classroom_id')->nullable();
            $table->integer('package_id')->nullable();
            $table->date('register_at');
            $table->date('started_at')->nullable();
            $table->date('ended_at')->nullable();
            $table->date('archived_at')->nullable();
            $table->string('note')->nullable();
            $table->integer('status')->default(Student::STATUS_CUSTOMER);
            $table->longText('extra_information')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
