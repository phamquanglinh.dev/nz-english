<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Renew extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $guarded = ['id'];

    const RENEW = 'renew';
    const REPLACE = 'replace';

    const WAITING = 1;
    const ACCEPTED = 2;

    public static function methodOptions(): array
    {
        return [
            static::RENEW => __('app.label.renew.method_options.renew'),
            static::REPLACE => __('app.label.renew.method_options.replace')
        ];
    }

    public function Student(): BelongsTo
    {
        return $this->belongsTo(Student::class, 'student_id', 'id')->where('package_id', '!=', null);
    }

    public function Package(): BelongsTo
    {
        return $this->belongsTo(Package::class, 'package_id', 'id');
    }


}
