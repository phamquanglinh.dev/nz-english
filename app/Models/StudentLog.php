<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class StudentLog extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $table = 'student_logs';
    protected $guarded = ['id'];

    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function Student(): BelongsTo
    {
        return $this->belongsTo(Student::class, 'student_id');
    }
}
