<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\InvoiceRequest;
use App\Models\Invoice;
use App\Models\Student;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Validator;
use Prologue\Alerts\Facades\Alert;

/**
 * Class InvoiceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class InvoiceCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Invoice::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/invoice');
        CRUD::setEntityNameStrings(__('app.module.invoice'), "DS " . __('app.module.invoice'));
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('created_by')->label(__('app.label.invoice.created_by'))
            ->type('select')->model('App\Models\User')->entity('createdBy');
        CRUD::column('amount')->label(__('app.label.invoice.amount'))->suffix(' đ')->type('number');
        CRUD::column('created_time')->label(__('app.label.invoice.created_time'));
        CRUD::column('student_id')->label(__('app.label.invoice.student_id'));
        CRUD::column('method')->label(__('app.label.invoice.method'))->type('select_from_array')->options(Invoice::methodOptions());

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $input = $this->crud->getRequest()->input();

        CRUD::setValidation(InvoiceRequest::class);

        if (isset($input['student_id'])) {
            CRUD::field('student_id')->default($input['student_id'])->attributes([
                'readonly' => true
            ])->type('hidden');
        }else {
            CRUD::field('student_id')->label(__('app.label.invoice.student_id'));
        }
        CRUD::field('amount')->label(__('app.label.invoice.amount'));
        CRUD::field('created_by')->default(backpack_user()->{'id'})->type('hidden');
        CRUD::field('created_time')->type('datetime')->label(__('app.label.invoice.created_time'));
        CRUD::field('method')->type('select_from_array')->options(Invoice::methodOptions())->label(__('app.label.invoice.method'));
        CRUD::field('note')->label(__('app.label.invoice.note'));


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store(InvoiceRequest $invoiceRequest)
    {
        $input = $this->crud->getRequest()->input();

        /**
         * @var Student $student
         */
        $student = Student::query()->where('id', $input['student_id'])->firstOrFail();

        if ($student->unPaidAmount() < $input['amount']) {
            return redirect()->back()->withErrors([
                'amount' => __('app.label.invoice.out_of_value')
            ]);
        }

        $input['package_id'] = $student['package_id'];

        Invoice::query()->create($input);

        Alert::success('success');

        return redirect('/admin/invoice');
    }
}
