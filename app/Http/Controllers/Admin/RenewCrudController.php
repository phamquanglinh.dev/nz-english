<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\QuickInvoice;
use App\Http\Requests\RenewRequest;
use App\Models\Invoice;
use App\Models\Package;
use App\Models\Renew;
use App\Models\Student;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;

/**
 * Class RenewCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class RenewCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use QuickInvoice;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Renew::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/renew');
        CRUD::setEntityNameStrings(__('app.module.renew'), __('app.module.renew'));
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(RenewRequest::class);
        $student_id = request()->get('student_id');

        if ($student_id) {
            $student = Student::query()->where('id', $student_id)->firstOrFail();
            CRUD::addField([
                'name' => 'student_id',
                'label' => __('app.label.renew.student_id'),
                'type' => 'hidden',
                'value' => $student['id']
            ]);
            CRUD::addField([
                'name' => 'atl_student',
                'label' => "Học sinh",
                'value' => $student->renew_label,
                'type' => 'text',
                'attributes' => [
                    'disabled' => true
                ]
            ]);
        } else {
            CRUD::addField([
                'name' => 'student_id',
                'label' => __('app.label.renew.student_id'),
                'type' => 'select2',
                'model' => 'App\Models\Student',
                'entity' => 'Student',
                'attribute' => 'renew_label',
                'hint' => 'Khi gia hạn sẽ thay đổi ngày kết thúc dựa trên gói mà hs đó đã đăng ký.
            <br>Nếu gói hiện tại vẫn còn hạn (ngày hiện tại sớm hơn ngày kết thúc) sẽ cộng thêm số ngày tương ứng.
            <br>Nếu gói đã hết hạn có thể chọn lại ngày bắt đầu'
            ]);
        }

        CRUD::field('created_by')->type('hidden')->value(backpack_user()->id);

        CRUD::addField([
            'name' => 'student_started_at',
            'label' => __('app.label.student.started_at'),
            'type' => 'date',
            'hint' => '<b class="text-danger">Khi chọn giá trị cho trường này thì ngày bắt đầu gói sẽ bị thay đổi dù gói đã hết hạn hay chưa, lưu ý khi sử dụng</b>'
        ]);

        self::invoiceFields();
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function store()
    {
        $entry = $this->crud->getRequest()->input();
        /**
         * @var Student $student
         */
        $student = Student::query()->where('id', $entry['student_id'])->firstOrFail();

        Renew::query()->create([
            'student_id' => $entry['student_id'],
            'package_id' => $student['package_id'],
            'new_started_at' => $entry['student_started_at'] ?? $student['started_at'],
            'created_by' => backpack_user()->{'id'},
        ]);

        if (isset($entry['invoice'])) {
            try {
                $invoice = json_decode($entry['invoice'], 1)[0];

                if ($invoice['amount'] != '') {
                    Invoice::query()->create([
                        'package_id' => $student['package_id'],
                        'student_id' => $entry['student_id'],
                        'created_time' => Carbon::now(),
                        'amount' => $invoice['amount'],
                        'method' => $invoice['method'] ?? Invoice::CASH,
                        'note' => $invoice['note'],
                        'created_by' => backpack_user()->{'id'}
                    ]);
                }
            } catch (\Exception $exception) {
                dd($exception);
            }
        }

        $package = Package::query()->where('id', $student['package_id'])->firstOrFail();

        Student::query()->where('id', $student['id'])->update([
            'started_at' => $entry['student_started_at'] ?? $student['started_at'],
            'ended_at' => Carbon::parse($student['ended_at'])->addWeeks($package['original_weeks'])
        ]);

        return redirect()->to('admin/student');
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
