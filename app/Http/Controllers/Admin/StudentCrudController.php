<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\InlineCreateOperation;
use App\Http\Requests\StudentRequest;
use App\Models\Classroom;
use App\Models\Invoice;
use App\Models\Package;
use App\Models\Student;
use App\Models\StudentLog;
use Backpack\CRUD\app\Exceptions\BackpackProRequiredException;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Prologue\Alerts\Facades\Alert;

/**
 * Class StudentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StudentCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        store as traitStore;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     * @throws BackpackProRequiredException
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Student::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/student');
        CRUD::setEntityNameStrings(__('app.module.student'), 'DS ' . __('app.module.student'));
        $this->crud->enableDetailsRow();
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->addFilter();

        CRUD::column('name')->type('closure')->label(__('app.label.student.name'))->function(function ($entry) {
            $name = $entry->name;
            if ($entry->has_new_log) {
                return $name . '<i class="mx-2 text-danger font-weight-bold las la-bell"></i>';
            }

            return $name;
        });

        CRUD::column('phone')->label(__('app.label.student.phone'));
        CRUD::column('classroom_id')->label(__('app.label.student.classroom_id'));
        CRUD::column('package_id')->label(__('app.label.student.package_id'));
        CRUD::column('sale_day')->label(__('app.label.student.sale_day'))->type('date')->format('DD-MM-YYYY');
        CRUD::column('started_at')->label(__('app.label.student.started_at'))->type('date')->format('DD-MM-YYYY');
        CRUD::column('ended_at')->label(__('app.label.student.ended_at'))->type('date')->format('DD-MM-YYYY')->wrapper([
            'class' => function ($crud, $controller, $entry) {
                /**
                 * @var Student $entry
                 */
                return $entry->getRemainingTextColor();
            }
        ]);

        CRUD::column('payment_date')->label(__('app.label.student.payment_date'))->type('date')->format('DD-MM-YYYY')->wrapper([
            'class' => function ($crud, $controller, $entry) {
                /**
                 * @var Student $entry
                 */
                return $entry->getPaymentDateTextColor();
            }
        ]);

        CRUD::column('status')->label(__('app.label.student.status'))->type('select_from_array')->options(Student::statusOptions());
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(StudentRequest::class);

        CRUD::field('branch')->type('hidden')->value(0);

        CRUD::field('avatar')->label(__('app.label.student.avatar'))->type('image')->wrapper([
            'class' => 'col-md-6 col-12 mb-3'
        ]);

        CRUD::field('alt_avatar')->label(__('app.label.student.avatar'))->type('hidden')->wrapper([
            'class' => 'col-md-6 col-12 mb-3 hidden'
        ]);

        CRUD::field('name')->label(__('app.label.student.name'))->wrapper([
            'class' => 'col-md-6 col-12 mb-3'
        ]);
        CRUD::field('birthday')->label(__('app.label.student.birthday'))->wrapper([
            'class' => 'col-md-6 col-12 mb-3'
        ]);
        CRUD::field('phone')->label(__('app.label.student.phone'))->wrapper([
            'class' => 'col-md-6 col-12 mb-3'
        ]);

        CRUD::addField([
            'name' => 'classroom_id',
            'label' => __('app.label.student.classroom_id'),
            'type' => 'relationship',
            'entity' => 'classroom',
            'attribute' => 'name',
            'model' => 'App\Models\Classroom',
            'wrapper' => [
                'class' => 'col-md-6 col-12 mb-3'
            ],
//            'inline_create' => true
        ]);

        CRUD::field('register_at')->label(__('app.label.student.register_at'))->wrapper([
            'class' => 'col-md-6 col-12 mb-3'
        ]);
        CRUD::field('sale_day')->label(__('app.label.student.sale_day'))->wrapper([
            'class' => 'col-md-6 col-12 mb-3'
        ]);

        if (isManagerOfHigher()) {
            CRUD::field('status')->label(__('app.label.student.status'))->wrapper([
                'class' => 'col-md-6 col-12 mb-3'
            ])->type('select_from_array')->options(Student::statusOptions());
        } else {
            CRUD::field('status')->type('hidden');
        }

        CRUD::field('package_id')->label(__('app.label.student.package_id'))->wrapper([
            'class' => 'col-md-6 col-12 mb-3'
        ]);
        CRUD::field('payment_date')->type('date')->label(__('app.label.student.payment_date'))->wrapper([
            'class' => 'col-md-6 col-12 mb-3'
        ]);

        if (isManagerOfHigher()) {
            CRUD::field('started_at')->label(__('app.label.student.started_at'))->wrapper([
                'class' => 'col-md-6 col-12 mb-3'
            ]);

            CRUD::field('update_ended_at')->type('switch')->label(__('app.label.student.update_ended_at'));
        }

        CRUD::field('ended_at')->label(__('app.label.student.started_at'))->type('hidden');

        CRUD::addField([
            'name' => 'extra_information',
            'label' => __('app.label.student.extra_information'),
            'type' => 'repeatable',
            'new_item_label' => __('app.extra_information.new_item_label'),
            'fields' => [
                [
                    'name' => 'label',
                    'type' => 'text',
                    'label' => __('app.extra_information.label'),
                    'wrapper' => [
                        'class' => 'col-md-6 col-12 mb-3'
                    ]
                ],
                [
                    'name' => 'value',
                    'type' => 'text',
                    'label' => __('app.extra_information.value'),
                    'wrapper' => [
                        'class' => 'col-md-6 col-12 mb-3'
                    ]
                ],
            ]
        ]);

        CRUD::field('note')->label(__('app.label.student.note'))->type('textarea');

//        if ($this->crud->getCurrentOperation() == 'create') {
//            CRUD::addField([
//                'name' => 'create_invoices',
//                'label' => __('app.label.student.invoices'),
//                'type' => 'repeatable',
//                'new_item_label' => __('app.invoice.new_item_label'),
//                'max_rows' => 1,
//                'min_rows' => 1,
//                'fields' => [
//                    [
//                        'name' => 'amount',
//                        'type' => 'number',
//                        'label' => __('app.label.invoice.amount'),
//                        'wrapper' => [
//                            'class' => 'col-md-4 col-12 mb-3'
//                        ],
//                        'suffix' => 'đ'
//                    ],
//                    [
//                        'name' => 'created_time',
//                        'type' => 'datetime',
//                        'label' => __('app.label.invoice.created_time'),
//                        'wrapper' => [
//                            'class' => 'col-md-4 col-12 mb-3'
//                        ]
//                    ],
//                    [
//                        'name' => 'method',
//                        'type' => 'select_from_array',
//                        'options' => Invoice::methodOptions(),
//                        'label' => __('app.label.invoice.method'),
//                        'wrapper' => [
//                            'class' => 'col-md-4 col-12 mb-3'
//                        ]
//                    ],
//                    [
//                        'name' => 'note',
//                        'type' => 'textarea',
//                        'label' => __('app.label.invoice.note'),
//                        'wrapper' => [
//                            'class' => 'col-md-12 col-12 mb-3'
//                        ]
//                    ],
//                ]
//            ]);
//        }
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store(): RedirectResponse
    {
        $input = $this->crud->getRequest()->input();

//        $invoices = json_decode($input['create_invoices']??'[]', 1)[0] ?? null;
//
//        if (!$input['package_id'] && $invoices['amount'] && $invoices['created_time'] && $invoices['method']) {
//            return redirect()->back()->withErrors([
//                'package_id' => 'Gói học không được để trống khi hoá đơn đuọc tạo ra.'
//            ]);
//        }

        $this->traitStore();
        $studentId = $this->crud->getCurrentEntryId();
        #TODO

//        if (!$invoices['amount'] || !$invoices['created_time'] || !$invoices['method']) {
//            return redirect('admin/student');
//        }

//        Invoice::query()->create([
//            'student_id' => $studentId,
//            'amount' => $invoices['amount'],
//            'created_time' => $invoices['created_time'],
//            'method' => $invoices['method'],
//            'note' => $invoices['note'],
//            'created_by' => backpack_user()->id,
//            'package_id' => $input['package_id']
//        ]);

        return redirect('admin/student');
    }

    public function update(): array|RedirectResponse
    {
        $input = $this->crud->getRequest()->input();

        if (isset($input['update_ended_at'])) {
            $currentId = $this->crud->getCurrentEntryId();

            Student::query()->where('id', $currentId)->update([
                'ended_at' => null
            ]);
        }

        $this->traitUpdate();

        return redirect('admin/student');
    }

    public function showDetailsRow(int $id)
    {
        $student = Student::query()->where('id', $id)->first();
        $studentLogs = StudentLog::query()->where('student_id', $id)->orderBy('created_at', 'DESC')->get();

        if (!$student) {
            $student = Student::withTrashed()->where('id', $id)->first();

            return view("vendor.backpack.student.show-old-detail", [
                'student' => $student,
                'studentLogs' => $studentLogs
            ]);
        }

        return view('vendor.backpack.student.show-detail', [
            'student' => $student,
            'studentLogs' => $studentLogs
        ]);
    }

    public function mark($id, Request $request)
    {
        $type = $request->get('type');
        $note = $request->get('note') ?? '';

        StudentLog::query()->create([
            'user_id' => backpack_user()->{'id'},
            'student_id' => $id,
            'type' => __('app.action.student.' . $type),
            'note' => $note,
        ]);

        Alert::success('OK');

        return redirect()->back();
    }

    private function addFilter()
    {
        $this->crud->addFilter([
            'name' => 'status',
            'type' => 'select2',
            'label' => 'Trạng thái'
        ], function () {
            return [
                Student::STATUS_CUSTOMER => 'Đang tư vấn',
                Student::STATUS_LEARNING => 'Đang học',
                Student::EXPERIENCED => 'Sắp hết hạn gói học',
                Student::UNPAID_AMOUNT_REMAINING => 'Sắp đến hạn đóng học phí'
            ];
        }, function ($value) {
            if ($value == Student::EXPERIENCED) {
                $endDate = Carbon::now()->addDays(7)->toDateString();
                $this->crud->query->whereRaw("DATE(`ended_at`) <= '" . $endDate . "'");

//                dd($sql);

                return;
            }

            if ($value == Student::UNPAID_AMOUNT_REMAINING) {
                $endDate = Carbon::now()->addDays(7)->toDateString();
                $this->crud->query->whereRaw("DATE(`payment_date`) <= '" . $endDate . "'");

//                dd($sql);

                return;
            }

            $this->crud->query->where('status', $value);
        });

        $this->crud->addFilter([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Tên học sinh'
        ], false, function ($value) {
            $this->crud->query->where('name', "like", "%$value%");
        });

        $this->crud->addFilter([
            'name' => 'sale_day',
            'type' => 'date_range',
            'label' => 'Ngày tư vấn',
            'date_range_options' => [
                'timePicker' => true
            ]
        ], false, function ($value) {
            $dates = json_decode($value);
            $this->crud->query->where('sale_day', '>=', $dates->from);
            $this->crud->query->where('sale_day', '<=', $dates->to);
        });

        $this->crud->addFilter([
            'name' => 'phone',
            'type' => 'text',
            'label' => 'Số điện thoại'
        ], false, function ($value) {
            $this->crud->query->where('phone', "like", "%$value%");
        });

        $this->crud->addFilter([
            'name' => 'package_id',
            'type' => 'select2',
            'label' => 'Gói đã đăng ký'
        ], function () {
            return Package::query()->get()->pluck('name', 'id')->toArray();
        }, function ($value) {
            $this->crud->query->where('package_id', $value);
        });

        $this->crud->addFilter([
            'name' => 'classroom_id',
            'type' => 'select2',
            'label' => 'Lớp đang học'
        ], function () {
            return Classroom::query()->get()->pluck('name', 'id')->toArray();
        }, function ($value) {
            $this->crud->query->where('classroom_id', $value);
        });

        $this->crud->addFilter([
            'name' => 'has_new_log',
            'type' => 'simple',
            'label' => 'Có cập nhật thanh toán mới'
        ], false, function () {
            $this->crud->query->whereHas('StudentLogs', function (Builder $builder) {
                $builder->where('confirmed', 0);
            });
        });

        $this->crud->addFilter([
            'name' => 'old_student',
            'type' => 'simple',
            'label' => 'Học sinh cũ'
        ], false, function () {
            $this->crud->query->onlyTrashed();
        });
    }

    public function delete(int $id)
    {
        Student::query()->where('id', $id)->delete();
        return redirect()->back()->with('success');
    }

    public function restore(int $id)
    {
        Student::withTrashed()->where('id', $id)->restore();
        return redirect()->back()->with('success');
    }
}
