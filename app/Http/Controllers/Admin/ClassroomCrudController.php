<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ClassroomRequest;
use App\Models\Classroom;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ClassroomCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ClassroomCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Classroom::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/classroom');
        CRUD::setEntityNameStrings(__('app.module.classroom'), 'DS ' . __('app.module.classroom'));
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('name')->label(__('app.label.classroom.name'));

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ClassroomRequest::class);
        CRUD::field('branch')->type('hidden')->value(0);
        CRUD::field('name')->label(__('app.label.classroom.name'));
        CRUD::field('note')->label(__('app.label.classroom.note'))->default('');
        CRUD::addField([
            'name' => 'schedules',
            'label' => __('app.label.classroom.schedules'),
            'type' => 'repeatable',
            'new_item_label' =>  __('app.schedule.new_item_label'),
            'fields' => [
                [
                    'name' => 'week_day',
                    'label' => __('app.schedule.week_day'),
                    'type' => 'select_from_array',
                    'options' => Classroom::weekDayOptions(),
                    'wrapper' => [
                        'class' => 'col-md-4 col-12'
                    ]
                ],
                [
                    'name' => 'start_time',
                    'type' => 'time',
                    'label' => __('app.schedule.start_time'),
                    'wrapper' => [
                        'class' => 'col-md-4 col-12'
                    ]
                ],
                [
                    'name' => 'end_time',
                    'type' => 'time',
                    'label' => __('app.schedule.end_time'),
                    'wrapper' => [
                        'class' => 'col-md-4 col-12'
                    ]
                ],
            ]
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
