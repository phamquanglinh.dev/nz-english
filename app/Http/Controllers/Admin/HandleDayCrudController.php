<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\HandleDayRequest;
use App\Models\Classroom;
use App\Models\Student;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Prologue\Alerts\Facades\Alert;

/**
 * Class HandleDayCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class HandleDayCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation {
        create as traitCreate;
    }
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\HandleDay::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/handle-day');
        CRUD::setEntityNameStrings(__('app.module.handle-day'), __('app.module.handle-day'));
        $this->crud->denyAccess(['list', 'update', 'delete']);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(HandleDayRequest::class);

        CRUD::addField([
            'name' => 'classroom_ids',
            'label' => __('app.label.handle_day.classroom_ids'),
            'type' => 'relationship',
            'model' => 'App\Models\Classroom',
            'entity' => 'Classrooms',
        ]);

        CRUD::addField([
            'name' => 'student_ids',
            'label' => __('app.label.handle_day.student_ids'),
            'type' => 'relationship',
            'model' => 'App\Models\Student',
            'entity' => 'Students',
        ]);

        CRUD::addField([
            'name' => 'bonus_days',
            'label' => __('app.label.handle_day.bonus_days'),
            'type' => 'number'
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    public function store(HandleDayRequest $handleDayRequest)
    {
        $input = $this->crud->getRequest()->input();

        $input['student_ids'] = empty($input['student_ids']) ? [-1] : $input['student_ids'];
        $input['classroom_ids'] = empty($input['classroom_ids']) ? [-1] : $input['classroom_ids'];

        /**
         * @var Student[] $students
         */
        $students = Student::query()->whereIn('classroom_id', $input['classroom_ids'])->orWhereIn('id', $input['student_ids'])->get();

        foreach ($students as $student) {
            if ($student['classroom_id'] == null) {
                continue;
            }

            $this->handleBonusDayForStudent($student, $input['bonus_days']);
        }
        Alert::success('Thành công');
        return redirect('admin/student');
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    private function handleBonusDayForStudent(Student $student, int $bonusDays): void
    {
        /*
         * @var Classroom  $classroom
         */
        $classroom = Classroom::query()->where('id', $student['classroom_id'])->first();

        if (!$classroom) {
            return;
        }

        $schedules = json_decode(($classroom->getAttribute('schedules') ?? "[]"));

        $schedulesMappedWeekDay = array_column($schedules, 'week_day');

        $newEndDate = Carbon::parse($student['ended_at']);

        for ($i = 0; $i < $bonusDays; $i++) {
            do {
                $newEndDate->addDay();
                $dayOfWeek = $newEndDate->weekday();

                if ($dayOfWeek == 0) {
                    $dayOfWeek = 8;
                } else {
                    $dayOfWeek = $dayOfWeek + 1;
                }

            } while (!in_array($dayOfWeek, $schedulesMappedWeekDay));
        }

        Student::query()->where('id',$student['id'])->update([
            'ended_at' => $newEndDate
        ]);
    }
}
