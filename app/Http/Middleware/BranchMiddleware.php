<?php

namespace App\Http\Middleware;

use App\Models\Branch;
use App\Models\User;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BranchMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|RedirectResponse) $next
     * @return \Illuminate\Http\Response|RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (backpack_auth()->check()) {
            /**
             * @var User $user
             */
            $user = User::query()->where('id', backpack_user()->id)->first();

            if ($user->getAttribute('branch') == null) {
                return redirect()->to('admin/branch/select');
            }

            $branch = Branch::query()->where('id', $user->getAttribute('branch'))->first();

            if ($branch == null) {
                return redirect()->to('admin/branch/select');
            }
        }

        return $next($request);
    }
}
