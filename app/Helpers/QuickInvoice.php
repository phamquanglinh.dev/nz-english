<?php

namespace App\Helpers;

use App\Models\Invoice;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade;

trait QuickInvoice
{
    /**
     * @return void
     * amount
     * method
     * note
     */
    public static function invoiceFields(): void
    {
        CrudPanelFacade::addField([
            'name' => 'invoice',
            'label' => __('app.module.invoice'),
            'type' => 'repeatable',
            'max_rows' => 1,
            'fields' => [
                [
                    'name' => 'amount',
                    'label' => __('app.label.invoice.amount'),
                    'suffix' => 'đ',
                    'type' => 'number',
                    'wrapper' => [
                        'class' => 'col-md-6 mb-3'
                    ]
                ],
                [
                    'name' => 'method',
                    'label' => __('app.label.invoice.method'),
                    'type' => 'select_from_array',
                    'wrapper' => [
                        'class' => 'col-md-6 mb-3',
                    ],
                    'options' => Invoice::methodOptions()
                ],
                [
                    'name' => 'note',
                    'label' => __('app.label.invoice.note'),
                    'type' => 'textarea'
                ]
            ],
        ]);
    }
}
