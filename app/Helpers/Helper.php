<?php

use App\Models\User;

if (!function_exists('role')) {
    function role()
    {
        return backpack_user()->{'role'};
    }
}

if (! function_exists('isAdmin')) {
    function isAdmin(): bool
    {
        return backpack_user()->{'role'} == User::ROLE_ADMIN;
    }
}

if (! function_exists('isManagerOfHigher')) {
    function isManagerOfHigher(): bool
    {
        return in_array(backpack_user()->{'role'}, [User::ROLE_ADMIN, User::ROLE_MANAGER]);
    }
}


if (!function_exists('isStaffOfHigher')) {
    function isStaffOfHigher(): bool
    {
        return in_array(backpack_user()->{'role'}, [User::ROLE_ADMIN, User::ROLE_MANAGER, User::ROLE_STAFF]);
    }
}

if (!function_exists('backpack_pro')) {
    function backpack_pro(): bool
    {
        return true;
    }
}
