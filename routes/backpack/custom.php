<?php

use App\Http\Controllers\Admin\StudentCrudController;
use Illuminate\Support\Facades\Route;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array)config('backpack.base.web_middleware', 'web'),
        (array)config('backpack.base.middleware_key', 'admin'),
        ['branch']
    ),
    'namespace' => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('user', 'UserCrudController');
    Route::crud('branch', 'BranchCrudController');
    Route::crud('student', 'StudentCrudController');
    Route::crud('classroom', 'ClassroomCrudController');
    Route::crud('package', 'PackageCrudController');
    Route::crud('handle-day', 'HandleDayCrudController');
    Route::crud('invoice', 'InvoiceCrudController');
    Route::post('student/mark/{id}', [StudentCrudController::class, 'mark']);
    Route::crud('renew', 'RenewCrudController');
    Route::crud('student-log', 'StudentLogCrudController');
    Route::get('student-log/confirmed/{id}', 'StudentLogCrudController@confirmed');
    Route::get('student/delete/{id}', [StudentCrudController::class, 'delete']);
    Route::get('student/restore/{id}', [StudentCrudController::class, 'restore']);
}); // this should be the absolute last line of this file
